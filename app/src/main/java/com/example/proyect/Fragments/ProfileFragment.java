package com.example.proyect.Fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.proyect.Activities.MainActivity;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.R;

import java.util.ArrayList;
import java.util.List;


public class ProfileFragment extends Fragment {
    public static final String ENTITAT = "ENTITAT";
    private Entitat entitat;
    View v;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity mainActivity = (MainActivity) getActivity();
        v = inflater.inflate(R.layout.fragment_profile, container, false);
        // Recuperem la entitat del mainActivity per omplir les dades del Fragment.
        if (mainActivity.getMyEntitat() != null ) {
            entitat = mainActivity.getMyEntitat();
        }

        // Executem el metode per omplir la pantalla amb les dades
        SettersProfile();

        ViewPager viewPager = v.findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        TabLayout tabs = v.findViewById(R.id.tabLayout);
        tabs.setupWithViewPager(viewPager);

        return v;
    }

    // Metode per fer els setters de la pantalla.
    public void SettersProfile(){
        TextView nomEntitat = v.findViewById(R.id.nomEntitat);

        nomEntitat.setText(entitat.getNom());
    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new TabDemandesFragment(), "DEMANDES");
        adapter.addFragment(new TabEquipsFragment(), "EQUIPS");
        adapter.addFragment(new TabEntitatFragment(), "ENTITAT");
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
    }

}