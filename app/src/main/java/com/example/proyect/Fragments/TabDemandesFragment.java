package com.example.proyect.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.proyect.Activities.ConcedidaDetalls;
import com.example.proyect.Activities.DemandaDetalls;
import com.example.proyect.Activities.MainActivity;
import com.example.proyect.Adapters.AdapterActivitatsConcedides;
import com.example.proyect.Adapters.AdapterActivitatsDemanades;
import com.example.proyect.POJO.Activitats_concedides;
import com.example.proyect.POJO.Activitats_demanades;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.POJO.Equip;
import com.example.proyect.R;

import java.util.ArrayList;


public class TabDemandesFragment extends Fragment {
    Entitat entitat;
    RecyclerView recViewConcedides;
    RecyclerView recViewDemanades;
    ArrayList<Activitats_demanades> demanades;
    ArrayList<Activitats_concedides> concedides;
    ArrayList<Equip> equips;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tab_demandes, container, false);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        MainActivity mainActivity = (MainActivity) getActivity();
        if (MainActivity.getEntitat() != null) {
            entitat = MainActivity.getEntitat();
            equips = entitat.getEquips();

            ArrayList<Activitats_concedides> concedides = entitat.getActivitats_concedides();
            ArrayList<Activitats_demanades> demanades = entitat.getActivitats_demanades();
            AdapterActivitatsConcedides concedidesAdapter = new AdapterActivitatsConcedides(getContext(), concedides);
            AdapterActivitatsDemanades demanadesAdapter = new AdapterActivitatsDemanades(getContext(), demanades);
            recViewConcedides = view.findViewById(R.id.RecViewAcceptades);
            recViewConcedides.setHasFixedSize(true);
            recViewConcedides.setLayoutManager(new GridLayoutManager(getContext(), 1));
            recViewConcedides.setAdapter(concedidesAdapter);
            recViewDemanades = view.findViewById(R.id.RecViewPendents);
            recViewDemanades.setHasFixedSize(true);
            recViewDemanades.setLayoutManager(new GridLayoutManager(getContext(), 1));
            recViewDemanades.setAdapter(demanadesAdapter);

            concedidesAdapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getContext(), ConcedidaDetalls.class);
                    i.putExtra(ConcedidaDetalls.ACTIVITAT_CONCEDIDA, entitat.getActivitats_concedides().get(recViewConcedides.getChildAdapterPosition(v)));
                    getActivity().startActivity(i);
                }
            });

            demanadesAdapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getContext(), DemandaDetalls.class);
                    i.putExtra(DemandaDetalls.ACTIVITAT_DEMANADA, entitat.getActivitats_demanades().get(recViewDemanades.getChildAdapterPosition(v)));
                    getActivity().startActivityForResult(i, MainActivity.REQUEST_DEMANDA_DETALLS);
                }
            });

        }
        return view;
    }
}