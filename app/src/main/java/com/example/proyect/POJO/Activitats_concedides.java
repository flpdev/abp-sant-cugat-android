package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Activitats_concedides implements Parcelable {
    private int id;
    private String tipus;
    private int id_espai;
    private int id_equip;
    private int id_activitat_demanada;
    private int id_usuari;
    private String nom;
    private Equip equips;
    private Espai espais;

    private Activitats_demanades activitats_demanades;

    public Activitats_demanades getActivitats_demanades() {
        return activitats_demanades;
    }

    public Equip getEquips() {
        return equips;
    }

    public int getId_equip() {
        return id_equip;
    }



    private ArrayList<Horaris_activitats> horaris_activitats;

    public String getNom() {
        return nom;
    }

    public String getTipus() {
        return tipus;
    }

    public Espai getEspais() {
        return espais;
    }

    public ArrayList<Horaris_activitats> getHoraris_activitats() {
        return horaris_activitats;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.tipus);
        dest.writeInt(this.id_espai);
        dest.writeInt(this.id_equip);
        dest.writeInt(this.id_activitat_demanada);
        dest.writeInt(this.id_usuari);
        dest.writeString(this.nom);
        dest.writeParcelable(this.equips, flags);
        dest.writeParcelable(this.espais, flags);
        dest.writeParcelable(this.activitats_demanades, flags);
        dest.writeTypedList(this.horaris_activitats);
    }

    public Activitats_concedides() {
    }

    protected Activitats_concedides(Parcel in) {
        this.id = in.readInt();
        this.tipus = in.readString();
        this.id_espai = in.readInt();
        this.id_equip = in.readInt();
        this.id_activitat_demanada = in.readInt();
        this.id_usuari = in.readInt();
        this.nom = in.readString();
        this.equips = in.readParcelable(Equip.class.getClassLoader());
        this.espais = in.readParcelable(Espai.class.getClassLoader());
        this.activitats_demanades = in.readParcelable(Activitats_demanades.class.getClassLoader());
        this.horaris_activitats = in.createTypedArrayList(Horaris_activitats.CREATOR);
    }

    public static final Creator<Activitats_concedides> CREATOR = new Creator<Activitats_concedides>() {
        @Override
        public Activitats_concedides createFromParcel(Parcel source) {
            return new Activitats_concedides(source);
        }

        @Override
        public Activitats_concedides[] newArray(int size) {
            return new Activitats_concedides[size];
        }
    };
}
