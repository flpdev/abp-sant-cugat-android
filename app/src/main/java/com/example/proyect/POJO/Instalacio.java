package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Instalacio implements Parcelable {
    private int id;
    private int id_instalacio;
    private String nom;
    private boolean gestio_externa;
    private String direccio;
    private boolean borrat;
    private float latitud;
    private float longitud;
    private String imatge;

    private ArrayList<Espai> espais;

    public ArrayList<Espai> getEspais() {
        return espais;
    }

    public void setEspais(ArrayList<Espai> espais) {
        this.espais = espais;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_instalacio() {
        return id_instalacio;
    }

    public void setId_instalacio(int id_instalacio) {
        this.id_instalacio = id_instalacio;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isGestio_externa() {
        return gestio_externa;
    }

    public void setGestio_externa(boolean gestio_externa) {
        this.gestio_externa = gestio_externa;
    }

    public String getDireccio() {
        return direccio;
    }

    public void setDireccio(String direccio) {
        this.direccio = direccio;
    }

    public boolean isBorrat() {
        return borrat;
    }

    public void setBorrat(boolean borrat) {
        this.borrat = borrat;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

    public String getImatge() {
        return imatge;
    }

    public void setImatge(String imatge) {
        this.imatge = imatge;
    }

    public Instalacio() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.id_instalacio);
        dest.writeString(this.nom);
        dest.writeByte(this.gestio_externa ? (byte) 1 : (byte) 0);
        dest.writeString(this.direccio);
        dest.writeByte(this.borrat ? (byte) 1 : (byte) 0);
        dest.writeFloat(this.latitud);
        dest.writeFloat(this.longitud);
        dest.writeString(this.imatge);
        dest.writeTypedList(this.espais);
    }

    protected Instalacio(Parcel in) {
        this.id = in.readInt();
        this.id_instalacio = in.readInt();
        this.nom = in.readString();
        this.gestio_externa = in.readByte() != 0;
        this.direccio = in.readString();
        this.borrat = in.readByte() != 0;
        this.latitud = in.readFloat();
        this.longitud = in.readFloat();
        this.imatge = in.readString();
        this.espais = in.createTypedArrayList(Espai.CREATOR);
    }

    public static final Creator<Instalacio> CREATOR = new Creator<Instalacio>() {
        @Override
        public Instalacio createFromParcel(Parcel source) {
            return new Instalacio(source);
        }

        @Override
        public Instalacio[] newArray(int size) {
            return new Instalacio[size];
        }
    };
}
