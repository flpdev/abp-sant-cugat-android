package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Time;

public class Horaris_adients implements Parcelable {
    private Integer id;
    private int id_dia_setmana;
    private Time hora_inici;
    private Time hora_final;
    private Integer id_activitat_demanada;

    private Dies_setmana dies_setmana;

    public Horaris_adients() {
    }

    public Horaris_adients(int id_dia_setmana, Time hora_inici, Time hora_final) {
        this.id_dia_setmana = id_dia_setmana;
        this.hora_inici = hora_inici;
        this.hora_final = hora_final;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getId_dia_setmana() {
        return id_dia_setmana;
    }

    public void setId_dia_setmana(int id_dia_setmana) {
        this.id_dia_setmana = id_dia_setmana;
    }

    public Time getHora_inici() {
        return hora_inici;
    }

    public void setHora_inici(Time hora_inici) {
        this.hora_inici = hora_inici;
    }

    public Time getHora_final() {
        return hora_final;
    }

    public void setHora_final(Time hora_final) {
        this.hora_final = hora_final;
    }

    public Integer getId_activitat_demanada() {
        return id_activitat_demanada;
    }

    public void setId_activitat_demanada(Integer id_activitat_demanada) {
        this.id_activitat_demanada = id_activitat_demanada;
    }

    public Dies_setmana getDies_setmana() {
        return dies_setmana;
    }

    public void setDies_setmana(Dies_setmana dies_setmana) {
        this.dies_setmana = dies_setmana;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeInt(this.id_dia_setmana);
        dest.writeSerializable(this.hora_inici);
        dest.writeSerializable(this.hora_final);
        dest.writeValue(this.id_activitat_demanada);
        dest.writeParcelable(this.dies_setmana, flags);
    }

    protected Horaris_adients(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id_dia_setmana = in.readInt();
        this.hora_inici = (Time) in.readSerializable();
        this.hora_final = (Time) in.readSerializable();
        this.id_activitat_demanada = (Integer) in.readValue(Integer.class.getClassLoader());
        this.dies_setmana = in.readParcelable(Dies_setmana.class.getClassLoader());
    }

    public static final Creator<Horaris_adients> CREATOR = new Creator<Horaris_adients>() {
        @Override
        public Horaris_adients createFromParcel(Parcel source) {
            return new Horaris_adients(source);
        }

        @Override
        public Horaris_adients[] newArray(int size) {
            return new Horaris_adients[size];
        }
    };
}
