package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

public class Competicions implements Parcelable {
    private int id;
    private String nom;

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nom);
    }

    public Competicions() {
    }

    protected Competicions(Parcel in) {
        this.id = in.readInt();
        this.nom = in.readString();
    }

    public static final Parcelable.Creator<Competicions> CREATOR = new Parcelable.Creator<Competicions>() {
        @Override
        public Competicions createFromParcel(Parcel source) {
            return new Competicions(source);
        }

        @Override
        public Competicions[] newArray(int size) {
            return new Competicions[size];
        }
    };
}
