package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

public class Categories_competicio implements Parcelable {
    private int id;
    private String nom;

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nom);
    }

    public Categories_competicio() {
    }

    protected Categories_competicio(Parcel in) {
        this.id = in.readInt();
        this.nom = in.readString();
    }

    public static final Parcelable.Creator<Categories_competicio> CREATOR = new Parcelable.Creator<Categories_competicio>() {
        @Override
        public Categories_competicio createFromParcel(Parcel source) {
            return new Categories_competicio(source);
        }

        @Override
        public Categories_competicio[] newArray(int size) {
            return new Categories_competicio[size];
        }
    };
}
