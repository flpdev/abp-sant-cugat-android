package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Activitats_demanades implements Parcelable {
    private Integer id;
    private String nom;
    private String tipus;
    private int id_espai;
    private int id_equip;
    private int durada;
    private int dies;
    private boolean es_assignada;
    private Equip equips;



    private Espai espais;
    private ArrayList<Horaris_adients> horaris_adients;

    public Activitats_demanades(String nom, String tipus, int durada, int dies, Espai espais, ArrayList<Horaris_adients> horaris_adients) {
        this.nom = nom;
        this.tipus = tipus;
        this.durada = durada;
        this.dies = dies;
        this.espais = espais;
        this.horaris_adients = horaris_adients;
    }

    public Equip getEquips() {
        return equips;
    }

    public String getNom() {
        return nom;
    }

    public String getTipus() {
        return tipus;
    }

    public Integer getId() {
        return id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public int getId_espai() {
        return id_espai;
    }

    public void setId_espai(int id_espai) {
        this.id_espai = id_espai;
    }

    public int getId_equip() {
        return id_equip;
    }

    public void setId_equip(int id_equip) {
        this.id_equip = id_equip;
    }

    public int getDurada() {
        return durada;
    }

    public void setDurada(int durada) {
        this.durada = durada;
    }

    public int getDies() {
        return dies;
    }

    public void setDies(int dies) {
        this.dies = dies;
    }

    public boolean isEs_assignada() {
        return es_assignada;
    }

    public void setEs_assignada(boolean es_assignada) {
        this.es_assignada = es_assignada;
    }

    public Espai getEspais() {
        return espais;
    }

    public void setEspais(Espai espais) {
        this.espais = espais;
    }

    public ArrayList<Horaris_adients> getHoraris_adients() {
        return horaris_adients;
    }

    public void setHoraris_adients(ArrayList<Horaris_adients> horaris_adients) {
        this.horaris_adients = horaris_adients;
    }

    public Activitats_demanades() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.nom);
        dest.writeString(this.tipus);
        dest.writeInt(this.id_espai);
        dest.writeInt(this.id_equip);
        dest.writeInt(this.durada);
        dest.writeInt(this.dies);
        dest.writeByte(this.es_assignada ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.equips, flags);
        dest.writeParcelable(this.espais, flags);
        dest.writeTypedList(this.horaris_adients);
    }

    protected Activitats_demanades(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nom = in.readString();
        this.tipus = in.readString();
        this.id_espai = in.readInt();
        this.id_equip = in.readInt();
        this.durada = in.readInt();
        this.dies = in.readInt();
        this.es_assignada = in.readByte() != 0;
        this.equips = in.readParcelable(Equip.class.getClassLoader());
        this.espais = in.readParcelable(Espai.class.getClassLoader());
        this.horaris_adients = in.createTypedArrayList(Horaris_adients.CREATOR);
    }

    public static final Creator<Activitats_demanades> CREATOR = new Creator<Activitats_demanades>() {
        @Override
        public Activitats_demanades createFromParcel(Parcel source) {
            return new Activitats_demanades(source);
        }

        @Override
        public Activitats_demanades[] newArray(int size) {
            return new Activitats_demanades[size];
        }
    };
}
