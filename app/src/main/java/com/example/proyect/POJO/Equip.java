package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Equip implements Parcelable {
    private Integer id;
    private String nom;
    private int id_entitat;
    private int id_categoria;
    private int id_esport;
    private int id_competicio;
    private Integer id_categoria_competicio;
    private int id_sexe;
    private boolean borrat;

    private ArrayList<Activitats_concedides> activitats_concedides;
    private ArrayList<Activitats_demanades> activitats_demanades;
    private Categoria categories;
    private Categories_competicio categories_competicio;
    private Competicions competicions;
    private Entitat entitats;
    private Esport esports;
    private Sexe sexes;

    public void clearRelationships() {
        activitats_concedides = new ArrayList<>();
        activitats_demanades = new ArrayList<>();
        categories = null;
        categories_competicio = null;
        competicions = null;
        entitats = null;
        esports = null;
        sexes = null;
    }

    public Integer getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getId_entitat() {
        return id_entitat;
    }

    public void setId_entitat(int id_entitat) {
        this.id_entitat = id_entitat;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public int getId_esport() {
        return id_esport;
    }

    public void setId_esport(int id_esport) {
        this.id_esport = id_esport;
    }

    public int getId_competicio() {
        return id_competicio;
    }

    public void setId_competicio(int id_competicio) {
        this.id_competicio = id_competicio;
    }

    public Integer getId_categoria_competicio() {
        return id_categoria_competicio;
    }

    public void setId_categoria_competicio(Integer id_categoria_competicio) {
        this.id_categoria_competicio = id_categoria_competicio;
    }

    public int getId_sexe() {
        return id_sexe;
    }

    public void setId_sexe(int id_sexe) {
        this.id_sexe = id_sexe;
    }

    public boolean isBorrat() {
        return borrat;
    }

    public void setBorrat(boolean borrat) {
        this.borrat = borrat;
    }

    public ArrayList<Activitats_concedides> getActivitats_concedides() {
        return activitats_concedides;
    }

    public void setActivitats_concedides(ArrayList<Activitats_concedides> activitats_concedides) {
        this.activitats_concedides = activitats_concedides;
    }

    public ArrayList<Activitats_demanades> getActivitats_demanades() {
        return activitats_demanades;
    }

    public void setActivitats_demanades(ArrayList<Activitats_demanades> activitats_demanades) {
        this.activitats_demanades = activitats_demanades;
    }

    public Categoria getCategories() {
        return categories;
    }

    public void setCategories(Categoria categories) {
        this.categories = categories;
    }

    public Categories_competicio getCategories_competicio() {
        return categories_competicio;
    }

    public void setCategories_competicio(Categories_competicio categories_competicio) {
        this.categories_competicio = categories_competicio;
    }

    public Competicions getCompeticions() {
        return competicions;
    }

    public void setCompeticions(Competicions competicions) {
        this.competicions = competicions;
    }

    public Entitat getEntitats() {
        return entitats;
    }

    public void setEntitats(Entitat entitats) {
        this.entitats = entitats;
    }

    public Esport getEsports() {
        return esports;
    }

    public void setEsports(Esport esports) {
        this.esports = esports;
    }

    public Sexe getSexes() {
        return sexes;
    }

    public void setSexes(Sexe sexes) {
        this.sexes = sexes;
    }

    public Equip() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.nom);
        dest.writeInt(this.id_entitat);
        dest.writeInt(this.id_categoria);
        dest.writeInt(this.id_esport);
        dest.writeInt(this.id_competicio);
        dest.writeValue(this.id_categoria_competicio);
        dest.writeInt(this.id_sexe);
        dest.writeByte(this.borrat ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.activitats_concedides);
        dest.writeTypedList(this.activitats_demanades);
        dest.writeParcelable(this.categories, flags);
        dest.writeParcelable(this.categories_competicio, flags);
        dest.writeParcelable(this.competicions, flags);
        dest.writeParcelable(this.entitats, flags);
        dest.writeParcelable(this.esports, flags);
        dest.writeParcelable(this.sexes, flags);
    }

    protected Equip(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nom = in.readString();
        this.id_entitat = in.readInt();
        this.id_categoria = in.readInt();
        this.id_esport = in.readInt();
        this.id_competicio = in.readInt();
        this.id_categoria_competicio = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id_sexe = in.readInt();
        this.borrat = in.readByte() != 0;
        this.activitats_concedides = in.createTypedArrayList(Activitats_concedides.CREATOR);
        this.activitats_demanades = in.createTypedArrayList(Activitats_demanades.CREATOR);
        this.categories = in.readParcelable(Categoria.class.getClassLoader());
        this.categories_competicio = in.readParcelable(Categories_competicio.class.getClassLoader());
        this.competicions = in.readParcelable(Competicions.class.getClassLoader());
        this.entitats = in.readParcelable(Entitat.class.getClassLoader());
        this.esports = in.readParcelable(Esport.class.getClassLoader());
        this.sexes = in.readParcelable(Sexe.class.getClassLoader());
    }

    public static final Creator<Equip> CREATOR = new Creator<Equip>() {
        @Override
        public Equip createFromParcel(Parcel source) {
            return new Equip(source);
        }

        @Override
        public Equip[] newArray(int size) {
            return new Equip[size];
        }
    };
}
