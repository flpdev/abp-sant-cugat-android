package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Time;

public class Horaris_activitats implements Parcelable {
    private int id;
    private int id_dia_setmana;
    private Time hora_inici;
    private Time hora_final;
    private int id_activitat_concedida;

    private Dies_setmana dies_setmana;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_dia_setmana() {
        return id_dia_setmana;
    }

    public void setId_dia_setmana(int id_dia_setmana) {
        this.id_dia_setmana = id_dia_setmana;
    }

    public Time getHora_inici() {
        return hora_inici;
    }

    public void setHora_inici(Time hora_inici) {
        this.hora_inici = hora_inici;
    }

    public Time getHora_final() {
        return hora_final;
    }

    public void setHora_final(Time hora_final) {
        this.hora_final = hora_final;
    }

    public int getId_activitat_concedida() {
        return id_activitat_concedida;
    }

    public void setId_activitat_concedida(int id_activitat_concedida) {
        this.id_activitat_concedida = id_activitat_concedida;
    }

    public Dies_setmana getDies_setmana() {
        return dies_setmana;
    }

    public void setDies_setmana(Dies_setmana dies_setmana) {
        this.dies_setmana = dies_setmana;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.id_dia_setmana);
        dest.writeSerializable(this.hora_inici);
        dest.writeSerializable(this.hora_final);
        dest.writeInt(this.id_activitat_concedida);
        dest.writeParcelable(this.dies_setmana, flags);
    }

    protected Horaris_activitats(Parcel in) {
        this.id = in.readInt();
        this.id_dia_setmana = in.readInt();
        this.hora_inici = (Time) in.readSerializable();
        this.hora_final = (Time) in.readSerializable();
        this.id_activitat_concedida = in.readInt();
        this.dies_setmana = in.readParcelable(Dies_setmana.class.getClassLoader());
    }

    public static final Creator<Horaris_activitats> CREATOR = new Creator<Horaris_activitats>() {
        @Override
        public Horaris_activitats createFromParcel(Parcel source) {
            return new Horaris_activitats(source);
        }

        @Override
        public Horaris_activitats[] newArray(int size) {
            return new Horaris_activitats[size];
        }
    };
}
