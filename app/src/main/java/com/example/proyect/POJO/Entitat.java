package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Entitat implements Parcelable {
    private int id;
    private String nom;
    private String direccio;
    private String cif;
    private int id_temporada;
    private String correu;
    private String facebook;
    private String instagram;
    private String twitter;
    private String password;
    private boolean borrat;
    private float latitud;
    private float longitud;
    private String imatge;
    private String video;

    private ArrayList<Telefon> telefons;
    private ArrayList<Equip> equips;

    public int getId_temporada() {
        return id_temporada;
    }

    public void setId_temporada(int id_temporada) {
        this.id_temporada = id_temporada;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isBorrat() {
        return borrat;
    }

    public void setBorrat(boolean borrat) {
        this.borrat = borrat;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

    public String getImatge() {
        return imatge;
    }

    public void setImatge(String imatge) {
        this.imatge = imatge;
    }

    public void setTelefons(ArrayList<Telefon> telefons) {
        this.telefons = telefons;
    }

    public void setEquips(ArrayList<Equip> equips) {
        this.equips = equips;
    }

    public ArrayList<Activitats_concedides> getActivitats_concedides() {
        ArrayList<Activitats_concedides> actiC = new ArrayList();
        if (equips != null) {
            for (Equip e : equips) {
                if (e.getActivitats_concedides() != null) {
                    actiC.addAll(e.getActivitats_concedides());
                }
            }
        }
        return actiC;
    }

    public ArrayList<Activitats_demanades> getActivitats_demanades() {
        ArrayList<Activitats_demanades> actiD = new ArrayList();
        if (equips != null) {
            for (Equip e : equips) {
                if (e.getActivitats_concedides() != null) {
                    actiD.addAll(e.getActivitats_demanades());
                }
            }
        }
        return actiD;
    }

    public Equip getEquipById(int id) {
        for (Equip e : equips) {
            if (e.getId() == id) {
                return e;
            }
        }
        return null;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDireccio() {
        return direccio;
    }

    public void setDireccio(String direccio) {
        this.direccio = direccio;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCorreu() {
        return correu;
    }

    public void setCorreu(String correu) {
        this.correu = correu;
    }

    public String getPassword() {
        return password;
    }

    public float getLatitud() {
        return latitud;
    }

    public float getLongitud() {
        return longitud;
    }

    public ArrayList<Telefon> getTelefons() {
        return telefons;
    }

    public ArrayList<Equip> getEquips() {
        return equips;
    }

    public Entitat() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nom);
        dest.writeString(this.direccio);
        dest.writeString(this.cif);
        dest.writeInt(this.id_temporada);
        dest.writeString(this.correu);
        dest.writeString(this.facebook);
        dest.writeString(this.instagram);
        dest.writeString(this.twitter);
        dest.writeString(this.password);
        dest.writeByte(this.borrat ? (byte) 1 : (byte) 0);
        dest.writeFloat(this.latitud);
        dest.writeFloat(this.longitud);
        dest.writeString(this.imatge);
        dest.writeString(this.video);
        dest.writeTypedList(this.telefons);
        dest.writeTypedList(this.equips);
    }

    protected Entitat(Parcel in) {
        this.id = in.readInt();
        this.nom = in.readString();
        this.direccio = in.readString();
        this.cif = in.readString();
        this.id_temporada = in.readInt();
        this.correu = in.readString();
        this.facebook = in.readString();
        this.instagram = in.readString();
        this.twitter = in.readString();
        this.password = in.readString();
        this.borrat = in.readByte() != 0;
        this.latitud = in.readFloat();
        this.longitud = in.readFloat();
        this.imatge = in.readString();
        this.video = in.readString();
        this.telefons = in.createTypedArrayList(Telefon.CREATOR);
        this.equips = in.createTypedArrayList(Equip.CREATOR);
    }

    public static final Creator<Entitat> CREATOR = new Creator<Entitat>() {
        @Override
        public Entitat createFromParcel(Parcel source) {
            return new Entitat(source);
        }

        @Override
        public Entitat[] newArray(int size) {
            return new Entitat[size];
        }
    };
}