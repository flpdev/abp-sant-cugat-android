package com.example.proyect.API.Services;

import com.example.proyect.POJO.Entitat;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface EntitatService {
    @GET("api/entitats")
    Call<ArrayList<Entitat>> getEntitats();

    @GET("api/entitats/{id}")
    Call<Entitat> getEntitat(@Path("id") int id);

    @POST("api/entitats")
    Call<Entitat> newEntitat(@Body Entitat entitat);

    @DELETE("api/entitats/{id}")
    Call<Entitat> deleteEntitat(@Path("id") int id);

    @PUT("api/entitats/{id}")
    Call<ResponseBody> updateEntitat(@Path("id") int id, @Body Entitat entitat);

    @GET("api/entitatCorreu")
    Call<Entitat> getEntitatCorreu(@Query("correu") String correu);

}
