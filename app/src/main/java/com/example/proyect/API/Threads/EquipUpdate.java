package com.example.proyect.API.Threads;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.EquipService;
import com.example.proyect.POJO.Equip;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class EquipUpdate extends Thread {
    private Equip equip;
    private Response<ResponseBody> response;

    public EquipUpdate(Equip equip) {
        this.equip = equip;
    }

    public Response<ResponseBody> getResponse() {
        return response;
    }

    @Override
    public void run() {
        EquipService service = API.getApi().create(EquipService.class);

        Call<ResponseBody> updateEquip = service.updateEquip(equip.getId(), equip);

        try {
            response = updateEquip.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}