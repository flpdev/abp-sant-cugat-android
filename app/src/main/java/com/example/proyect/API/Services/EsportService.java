package com.example.proyect.API.Services;

import com.example.proyect.POJO.Esport;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface EsportService {

    @GET("api/esports")
    Call<ArrayList<Esport>> getEsports();

    @GET("api/esports/{id}")
    Call<Esport> getEsport(@Path("id") int id);

    @POST("api/esports")
    Call<Esport> newEsport(@Body Esport esport);

    @DELETE("api/esports/{id}")
    Call<Esport> deleteEsport(@Path("id") int id);

    @PUT("api/esports/{id}")
    Call<ResponseBody> updateEsport(@Path("id") int id, @Body Esport esport);
}
