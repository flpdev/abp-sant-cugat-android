package com.example.proyect.API.Services;

import com.example.proyect.POJO.Activitats_demanades;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ActivitatDemanadaService {
    @POST("api/activitats_demanades")
    Call<Activitats_demanades> newActivitat(@Body Activitats_demanades activitat);

    @DELETE("api/activitats_demanades/{id}")
    Call<Activitats_demanades> deleteActivitat(@Path("id") int id);
}
