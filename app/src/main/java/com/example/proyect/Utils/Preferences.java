package com.example.proyect.Utils;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.DiesSetmanaService;
import com.example.proyect.POJO.Dies_setmana;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.R;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class Preferences extends Application {

	private static SharedPreferences settings;


	private static final String PREFS_NAME = "MyPrefsFile";
	private static final String FIRST_TIME_STR = "first_time";
	private static final String DARK_THEME = "dark_theme";
	private static final String USER = "user";
	private static final String LOGGED_IN = "isLoggedIn";
	private static Entitat entitat;

	/**
	 * @return un boolea, que fa referencia a si s'ha guardat el tema obscur a les preferencies
	 * @since 1.0
	 */
	public static boolean isDarkModeSaved(){

		return settings.getBoolean(DARK_THEME, false);
	}

	public static void saveThemePreferences(boolean isDarkTheme){
		settings.edit().putBoolean(DARK_THEME, isDarkTheme).apply();
	}

	public static boolean isFirstTime(){

		return settings.getBoolean(FIRST_TIME_STR, true);
	}

	public static void saveFirstTimePreferences(){

		settings.edit().putBoolean(FIRST_TIME_STR, false).apply();
	}

	public static void setSettings(SharedPreferences settings) {

		Preferences.settings = settings;


	}

	public static String getPrefsName() {
		return PREFS_NAME;
	}

	public static void restartActivityAfterThemeChanged(Activity context){
		context.recreate();
	}

	public static void setTheme(Activity context){
		if(Preferences.isDarkModeSaved()){
			context.setTheme(R.style.mainStyleDark);
		}else {
			context.setTheme(R.style.mainStyle);
		}
	}

	public static void saveUser(int userID){
		settings.edit().putInt(USER, userID).apply();
		settings.edit().putBoolean(LOGGED_IN, true).apply();

	}

	public static boolean isUserLoggedIn(){
		return settings.getBoolean(LOGGED_IN, false);
	}

	public static void logout(){
		settings.edit().putBoolean(LOGGED_IN, false).apply();
		settings.edit().remove(USER).apply();
	}

	public static int getUserID(){
		return settings.getInt(USER, 0);
	}

	public static void saveEntitat(Entitat e){
		entitat = e;
	}

	public static Entitat getEntitat(){
		return entitat;
	}

	public static void setIsEntityRetrieved(boolean isEntityRetrieved){
		settings.edit().putBoolean("isEntityRetrieved", isEntityRetrieved).apply();
	}

	public static ArrayList<Dies_setmana> getDiesSetmana() {
		GetDiesThread proc = new GetDiesThread();

		proc.start();

		try {
			proc.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return proc.response.body();
	}

	public static class GetDiesThread extends Thread {
		Response<ArrayList<Dies_setmana>> response;
		@Override
		public void run() {
			DiesSetmanaService diesSetmanaService = API.getApi().create(DiesSetmanaService.class);

			Call<ArrayList<Dies_setmana>> callDies = diesSetmanaService.getDies();

			try {
				response = callDies.execute();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
