package com.example.proyect.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.proyect.Adapters.HorariAdapter;
import com.example.proyect.POJO.Activitats_concedides;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.R;

public class ConcedidaDetalls extends AppCompatActivity {
    public static final String ACTIVITAT_CONCEDIDA = "activitat_concedida";

    private Activitats_concedides activitat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demanda_detalls);

        Entitat entitat = MainActivity.getEntitat();
        activitat = (Activitats_concedides) getIntent().getExtras().get(ACTIVITAT_CONCEDIDA);

        TextView activitatName = findViewById(R.id.activitatName);
        TextView espai = findViewById(R.id.espaiName);
        TextView instalacio = findViewById(R.id.instalacioName);
        TextView equip = findViewById(R.id.equipName);
        TextView dies = findViewById(R.id.numDies);
        TextView hores = findViewById(R.id.numHores);
        TextView horariTitle = findViewById(R.id.horariTitle);
        TextView tipus = findViewById(R.id.tipus);

        activitatName.setText(activitat.getNom());
        horariTitle.setText("Horaris activitat");
        espai.setText(activitat.getEspais().getNom());
        instalacio.setText(activitat.getEspais().getInstalacions().getNom());
        equip.setText(entitat.getEquipById(activitat.getId_equip()).getNom());
        dies.setText(activitat.getActivitats_demanades().getDies() + (activitat.getActivitats_demanades().getDies() > 1 ? " Dies" : " Dia"));
        hores.setText(activitat.getActivitats_demanades().getDurada() + (activitat.getActivitats_demanades().getDurada() > 1 ? " Hores" : " Hora"));
        tipus.setText(activitat.getTipus());

        HorariAdapter adapter = new HorariAdapter(activitat.getHoraris_activitats(), true);

        RecyclerView list = findViewById(R.id.listHoraris);
        list.setHasFixedSize(true);
        list.setAdapter(adapter);
        list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

    }
}
