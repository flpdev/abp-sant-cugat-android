package com.example.proyect.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.ActivitatDemanadaService;
import com.example.proyect.API.Threads.EntityThread;
import com.example.proyect.Adapters.AdapterNovesDemandes;
import com.example.proyect.POJO.Activitats_demanades;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.POJO.Equip;
import com.example.proyect.POJO.Espai;
import com.example.proyect.POJO.Horaris_adients;
import com.example.proyect.POJO.Instalacio;
import com.example.proyect.R;
import com.example.proyect.Utils.Preferences;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EnviarDemanda extends AppCompatActivity implements AdapterNovesDemandes.OnItemClick {
    ArrayList<Horaris_adients> itemList = new ArrayList<>();
    Button buttonAfegir, buttonEnviar;
    RecyclerView recyclerView;
    AdapterNovesDemandes rvAdapter;
    TextView espai, instalacio, nom, tipus, durada, dies;
    Spinner equips;

    Entitat entitat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_demanda);
        Espai espaiObj = getIntent().getParcelableExtra(DemandaActivity.CREARDEMANDA);
        Instalacio instalacioObj = getIntent().getParcelableExtra(DemandaActivity.CREARINSTALACION);
        itemList.add(new Horaris_adients());
        recyclerView = findViewById(R.id.recyclerView);
        buttonAfegir = findViewById(R.id.buttonAfegir);
        buttonEnviar = findViewById(R.id.buttonEnviar);
        durada = findViewById(R.id.durada);
        dies = findViewById(R.id.dies);
        espai = findViewById(R.id.editTextEspai);
        instalacio = findViewById(R.id.editTextInstalacio);
        nom = findViewById(R.id.editTextNomActivitat);
        tipus = findViewById(R.id.editTextTipusActivitat);
        instalacio.setText(instalacioObj.getNom());
        espai.setText(espaiObj.getNom());
        //Setting the layout and Adapter for RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvAdapter = new AdapterNovesDemandes(itemList, EnviarDemanda.this);
        recyclerView.setAdapter(rvAdapter);

        entitat = MainActivity.getEntitat();

        equips = findViewById(R.id.spinnerEquips);

        ArrayList<String> nomsEquips = new ArrayList();

        for(Equip e: entitat.getEquips()) {
            nomsEquips.add(e.getNom());
        }

        ArrayAdapter<String> adapterEquips = new ArrayAdapter<>(EnviarDemanda.this, android.R.layout.simple_spinner_item, nomsEquips);
        equips.setAdapter(adapterEquips);

        buttonAfegir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddButtonClicked(v);
            }
        });

        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!nom.getText().toString().equals("") && !tipus.getText().toString().equals("") && !durada.getText().toString().equals("") && !dies.getText().toString().equals("")) {
                    Activitats_demanades activitat;
                    activitat = new Activitats_demanades(nom.getText().toString(), tipus.getText().toString(), Integer.parseInt(durada.getText().toString()), Integer.parseInt(dies.getText().toString()), espaiObj, itemList);
                    activitat.setId_equip(entitat.getEquips().get(equips.getSelectedItemPosition()).getId());

                    ActivitatDemanadaService service = API.getApi().create(ActivitatDemanadaService.class);

                    Call<Activitats_demanades> call = service.newActivitat(activitat);


                    call.enqueue(new Callback<Activitats_demanades>() {
                        @Override
                        public void onResponse(Call<Activitats_demanades> call, Response<Activitats_demanades> response) {
                            String[] responseSplit = String.valueOf(response.code()).split("");
                            if (responseSplit[1].equals("2")) {
                                setResult(RESULT_OK);

                                EntityThread t = new EntityThread(Preferences.getUserID());
                                t.start();
                                MainActivity.setEntityRetrieved(false);
                                try {
                                    t.join();
                                    MainActivity.setEntitat(t.getEntitat());
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                //finish();
                                Intent i = new Intent(EnviarDemanda.this, MainActivity.class);
                                startActivity(i);
                            } else {
                                try {
                                    Toast.makeText(EnviarDemanda.this, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Activitats_demanades> call, Throwable t) {
                            Toast.makeText(EnviarDemanda.this, t.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    if (nom.getText().toString().equals("")) {
                        nom.setHighlightColor(getResources().getColor(R.color.red));
                        nom.requestFocus();
                        nom.setBackgroundResource(R.drawable.shapes);
                        nom.setHint("Requerit");
                        nom.setHintTextColor(getResources().getColor(R.color.red));
                        Toast.makeText(v.getContext(), "No has escrit el nom de la activitat", Toast.LENGTH_SHORT).show();
                    } else if (tipus.getText().toString().equals("")) {
                        tipus.setHighlightColor(getResources().getColor(R.color.red));
                        tipus.requestFocus();
                        tipus.setBackgroundResource(R.drawable.shapes);
                        tipus.setHint("Requerit");
                        tipus.setHintTextColor(getResources().getColor(R.color.red));
                        Toast.makeText(v.getContext(), "No has escrit el tipus de la activitat", Toast.LENGTH_SHORT).show();
                        nom.setBackgroundResource(R.drawable.shapesoff);
                    } else if (durada.getText().toString().equals("")) {
                        durada.setHighlightColor(getResources().getColor(R.color.red));
                        durada.requestFocus();
                        durada.setBackgroundResource(R.drawable.shapes);
                        durada.setHint("Requerit");
                        durada.setHintTextColor(getResources().getColor(R.color.red));
                        Toast.makeText(v.getContext(), "No has escrit la durada de la activitat", Toast.LENGTH_SHORT).show();
                        nom.setBackgroundResource(R.drawable.shapesoff);
                        tipus.setBackgroundResource(R.drawable.shapesoff);
                    } else if (dies.getText().toString().equals("")) {
                        dies.setHighlightColor(getResources().getColor(R.color.red));
                        dies.requestFocus();
                        dies.setBackgroundResource(R.drawable.shapes);
                        dies.setHint("Requerit");
                        dies.setHintTextColor(getResources().getColor(R.color.red));
                        Toast.makeText(v.getContext(), "No has escrit els dies de la activitat", Toast.LENGTH_SHORT).show();
                        nom.setBackgroundResource(R.drawable.shapesoff);
                        tipus.setBackgroundResource(R.drawable.shapesoff);
                        durada.setBackgroundResource(R.drawable.shapesoff);
                    }
                }
            }
        });
    }

    //Click listener for "Add" Button
    public void onAddButtonClicked(View view) {

        try {
            itemList.add(new Horaris_adients());
            rvAdapter.notifyItemInserted(itemList.size() - 1);
        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "The field is empty",
                    Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(Horaris_adients horari, int position) {
        itemList.set(position, horari);
    }

    private static class NewActivitat extends Thread {
        public Activitats_demanades activitat;
        public Response<Activitats_demanades> response;

        public NewActivitat(Activitats_demanades activitat) {
            this.activitat = activitat;
        }

        @Override
        public void run() {
            ActivitatDemanadaService ser = API.getApi().create(ActivitatDemanadaService.class);
            Call<Activitats_demanades> call = ser.newActivitat(activitat);

            try {
                response = call.execute();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
}