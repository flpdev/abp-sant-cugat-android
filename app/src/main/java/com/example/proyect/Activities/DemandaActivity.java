package com.example.proyect.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.proyect.POJO.Espai;
import com.example.proyect.POJO.Instalacio;
import com.example.proyect.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DemandaActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static final String CREARDEMANDA = "CREARDEMANDA";
    public static final String CREARINSTALACION = "CREARINSTALACION";
    public static final String ENTITAT = "entitat";

    ImageView imageEspai;
    TextView nom, preu;
    Espai espai;
    Button button;
    Instalacio instalacio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demanda);
        if (getIntent().getParcelableExtra(InstalacioDetail.EXTRA_MESSAGE) != null) {
            espai = getIntent().getParcelableExtra(InstalacioDetail.EXTRA_MESSAGE);
            instalacio = getIntent().getParcelableExtra(InstalacioDetail.EXTRA_MESSAGE2);
        }

        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        omplirDemandaActivity();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DemandaActivity.this, EnviarDemanda.class);
                i.putExtra(CREARDEMANDA, espai);
                i.putExtra(CREARINSTALACION, instalacio);
                i.putExtra(ENTITAT, MainActivity.getEntitat());
                startActivity(i);
            }
        });
    }

    private void omplirDemandaActivity() {
        nom = findViewById(R.id.nomEspai);
        preu = findViewById(R.id.precioEspai);
        imageEspai = findViewById(R.id.imageView);
        button = findViewById(R.id.buttonCrearDemanda);
        imageEspai.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(espai.getImatge(), "drawable", getPackageName())));
        nom.setText(espai.getNom());
        preu.setText(String.format("%.2f €", espai.getPreu()));
    }


    // Include the OnCreate() method here too, as described above.
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // and move the map's camera to the same location.
        LatLng place = new LatLng(instalacio.getLatitud(), instalacio.getLongitud());
        googleMap.addMarker(new MarkerOptions().position(place)
                .title(instalacio.getNom()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(place));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
    }
}
