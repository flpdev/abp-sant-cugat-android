package com.example.proyect.Activities;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.proyect.Fragments.TabEntitatFragment;
import com.example.proyect.R;

public class PlayVideo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);

        String path = getIntent().getStringExtra(TabEntitatFragment.VIDEO_PLAY);
        VideoView videoView = findViewById(R.id.VideoPlay);

        videoView.setVideoPath(path);
        videoView.requestFocus();
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.start();

    }
}
