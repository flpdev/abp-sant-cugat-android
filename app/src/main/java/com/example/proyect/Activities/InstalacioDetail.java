package com.example.proyect.Activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.proyect.Adapters.EspaisAdapter;
import com.example.proyect.Fragments.StartDemandFragment;
import com.example.proyect.POJO.Espai;
import com.example.proyect.POJO.Instalacio;
import com.example.proyect.R;

import java.util.ArrayList;

public class InstalacioDetail extends AppCompatActivity
{
    private RecyclerView recyclerView;
    public static final String EXTRA_MESSAGE = "DETAIL";
    public static final String EXTRA_MESSAGE2 = "INSTALACIO";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instalacio_detail);

        Instalacio instalacio = getIntent().getParcelableExtra(StartDemandFragment.EXTRA_TEXTO);
        ArrayList espais = instalacio.getEspais();
        // Inicialitzo la RecyclerView

        recyclerView = findViewById(R.id.RecViewEspais);
        recyclerView.setHasFixedSize(true);
        final EspaisAdapter adapter = new EspaisAdapter(espais, this);

        // Posem el adapter a la RecyclerView

        recyclerView.setAdapter(adapter);

        // Li asignem un layout a la RecyclerView

        recyclerView.setLayoutManager(new GridLayoutManager(this,1));

        // Metode setOnClick

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemClick = recyclerView.getChildAdapterPosition(v);
                Espai espai = (Espai) espais.get(itemClick);

                Intent intent = new Intent(InstalacioDetail.this, DemandaActivity.class);
                intent.putExtra(EXTRA_MESSAGE, espai);
                intent.putExtra(EXTRA_MESSAGE2, instalacio);
                startActivity(intent);
            }
        });
    }
}
