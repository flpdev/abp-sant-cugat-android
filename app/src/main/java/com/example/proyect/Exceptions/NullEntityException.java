package com.example.proyect.Exceptions;
/**
 * Excepcio que s'utiltiza per quan lña entitat recuperada de la base de dades,
 * es per algun motiu null
 * @since 1.0
 */
public class NullEntityException extends Exception {

    private static final String MSG = "No s'ha pogut recuperar la entitat";

    public  NullEntityException(){
        super(MSG);
    }
}
