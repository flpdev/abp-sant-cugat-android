package com.example.proyect.Exceptions;


public class UserPasswordException extends Exception {

    private static final String MSG = "correu o contrasenya incorrectes";

    public UserPasswordException() {
        super(MSG);
    }


}
