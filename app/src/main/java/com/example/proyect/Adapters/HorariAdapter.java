package com.example.proyect.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.proyect.POJO.Horaris_activitats;
import com.example.proyect.POJO.Horaris_adients;
import com.example.proyect.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class HorariAdapter extends RecyclerView.Adapter<HorariAdapter.ViewHolder> {
    private ArrayList<Horaris_activitats> horaris_activitats;
    private ArrayList<Horaris_adients> horaris_adients;

    public HorariAdapter(ArrayList<Horaris_activitats> horaris_activitats, boolean useless) {
        this.horaris_activitats = horaris_activitats;
    }

    public HorariAdapter(ArrayList<Horaris_adients> horaris_adients) {
        this.horaris_adients = horaris_adients;
    }

    @NonNull
    @Override
    public HorariAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.horari_list_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HorariAdapter.ViewHolder viewHolder, int i) {
        if(horaris_activitats != null) {
            viewHolder.bindHorari(horaris_activitats.get(i));
        }

        if(horaris_adients != null) {
            viewHolder.bindHorari(horaris_adients.get(i));
        }
    }

    @Override
    public int getItemCount() {
        if(horaris_activitats != null) {
            return horaris_activitats.size();
        }

        if(horaris_adients != null) {
            return horaris_adients.size();
        }

        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        TextView dia;
        TextView horaStart;
        TextView horaEnd;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            dia = itemView.findViewById(R.id.diaName);
            horaStart = itemView.findViewById(R.id.horaStart);
            horaEnd = itemView.findViewById(R.id.horaEnd);
        }

        public void bindHorari(Horaris_activitats h) {
            dia.setText(h.getDies_setmana().getDia());
            horaStart.setText(dateFormat.format(h.getHora_inici()));
            horaEnd.setText(dateFormat.format(h.getHora_final()));
        }

        public void bindHorari(Horaris_adients h) {
            dia.setText(h.getDies_setmana().getDia());
            horaStart.setText(dateFormat.format(h.getHora_inici()));
            horaEnd.setText(dateFormat.format(h.getHora_final()));
        }
    }
}
