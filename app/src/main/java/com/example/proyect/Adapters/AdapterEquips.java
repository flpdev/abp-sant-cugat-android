package com.example.proyect.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.proyect.Activities.EditarEquip;
import com.example.proyect.Activities.MainActivity;
import com.example.proyect.POJO.Equip;
import com.example.proyect.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterEquips extends RecyclerView.Adapter<AdapterEquips.ViewHolder> {
    private List<Equip> equips;
    private Context mContext;

    // Constructor
    public AdapterEquips(Context context, ArrayList<Equip> equips) {
        this.equips = equips;
        this.mContext = context;
    }

    @NonNull
    @Override
    public AdapterEquips.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        view = LayoutInflater.from(mContext).inflate(R.layout.item_activitat, viewGroup, false);
        AdapterEquips.ViewHolder vHolder = new AdapterEquips.ViewHolder(view);

        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterEquips.ViewHolder viewHolder, int i) {
        viewHolder.name.setText(equips.get(i).getNom());
        viewHolder.address.setText(equips.get(i).getEsports().getNom());
        viewHolder.parentItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, EditarEquip.class);
                intent.putExtra(EditarEquip.EXTRA_EQUIP, equips.get(i));
                intent.putExtra(EditarEquip.CREATING_BOOL, false);
                ((MainActivity)mContext).startActivityForResult(intent, MainActivity.REQUEST_EDIT_EQUIP);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, address;
        private CardView parentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nomActivitat);
            address = itemView.findViewById(R.id.tipoActivitat);
            parentItem = itemView.findViewById(R.id.cardviewActivity);
        }
    }

    @Override
    public int getItemCount() {
        return equips.size();
    }
}
