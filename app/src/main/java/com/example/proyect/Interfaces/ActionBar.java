package com.example.proyect.Interfaces;

import android.app.Activity;
import android.view.WindowManager;

public interface ActionBar {

    static void hideActionBar(Activity context){
        context.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
